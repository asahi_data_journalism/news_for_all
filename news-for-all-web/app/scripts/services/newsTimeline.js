angular.module('newsForAllWebApp')
.constant('API_ENDPOINT', 'http://ec2-54-199-234-107.ap-northeast-1.compute.amazonaws.com/api')
.factory('newsTimeline', ['$http', '$q', 'API_ENDPOINT', function($http, $q){
	'use strict';
	var instance = {};
	var deferred = $q.defer();
	instance.getArticles = function(){
		$http.get('/scripts/news.json')
		.success(function(data, status){
			if(angular.isArray(data.articles)) {
				deferred.resolve(data.articles);
			}
		})
		.error(function(data, status){
			deferred.reject(data);
		});
		return deferred.promise;
	};
	return instance;
}]);