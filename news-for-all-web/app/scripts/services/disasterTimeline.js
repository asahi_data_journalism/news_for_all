angular.module('newsForAllWebApp')
.factory('disasterTimeline', ['$http', '$q', 'API_ENDPOINT', function($http, $q){
	'use strict';
	var instance = {};
	var deferred = $q.defer();
	instance.getEvents = function(){
		$http.get('/scripts/disaster.json')
		.success(function(data, status){
			if(angular.isArray(data.data)) {
				deferred.resolve(data.data);
			}
		})
		.error(function(data, status){
			deferred.reject(data);
		});
		return deferred.promise;
	};
	return instance;
}]);