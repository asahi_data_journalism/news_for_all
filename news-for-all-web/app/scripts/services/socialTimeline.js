angular.module('newsForAllWebApp')
.factory('socialTimeline', ['$http', '$q', 'API_ENDPOINT', function($http, $q){
	'use strict';
	var instance = {};
	var deferred = $q.defer();
	instance.getTweets = function(){
		$http.get('/scripts/twitter.json')
		.success(function(data, status){
			if(angular.isArray(data.tweets)) {
				deferred.resolve(data.tweets);
			} else {
				deferred.resolve([]);
			}
		})
		.error(function(data, status){
			deferred.reject(data);
		});
		return deferred.promise;
	};
	return instance;
}]);