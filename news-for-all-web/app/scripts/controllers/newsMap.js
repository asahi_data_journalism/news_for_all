angular.module('newsForAllWebApp')
.controller('NewsMapCtrl', function ($window, $scope, $timeout) {
	'use strict';
	var tokyoLatLng = new $window.google.maps.LatLng(35.3951, 139.4555);
	var sfLatLng = new $window.google.maps.LatLng(37.774546, -122.433523);

	var mapOptions = {
		center: sfLatLng,
		zoom: 13,
		styles: $window.midnightCommander
	};
	var map, pointArray, heatMap;
	var newsData = null;
	var initMap = function(){
		map = new $window.google.maps.Map(document.getElementById('map_view'),
			mapOptions);
		pointArray = new google.maps.MVCArray();
		heatMap = new google.maps.visualization.HeatmapLayer({
			data: pointArray
		});
		heatMap.setMap(map);

		$scope.$watch('frame', updatePoints);
		$window.google.maps.event.addListenerOnce(map, 'tilesloaded', function(){
			$scope.$emit('MapReady');
		});
	};

	var currentFrame = -1;

	var updatePoints = function(toFrame) {
		var begin = 0;
		var end = 0;
		if(currentFrame > 0) {
			begin = currentFrame + 1;
		} else {
			end = toFrame;
		}
		if(begin < end) {
			var arr = newsData.slice(begin, end);
			angular.forEach(arr, function(val){
				pointArray.push(val);
			});
		}
	};

	// tentative
	var initNewsData = function(){
		newsData = $window.taxiData;
	};
	initNewsData();

	$scope.$on('resetTimeline', function(){
		pointArray.clear();
		currentFrame = -1;
	});

	initMap();
});