angular.module('newsForAllWebApp')
.controller('DisasterMapCtrl', function ($window, $scope, $timeout, midnightCommander, $rootScope) {
	'use strict';
	var tokyoLatLng = new $window.google.maps.LatLng(35.9236798, 139.1581533);
	var mapOptions = {
		center: tokyoLatLng,
		zoom: 9,
		styles: midnightCommander
	};
	var map, markerArray, pointArray, socialPointArray, heatMap, socialHeatMap;
	var initMap = function(){
		var markerIconUrls = {
			isolated: {
				url: 'images/snow.png',
				size: new google.maps.Size(32, 32)
			},
			blackout: {
				url: 'images/electro_32.png',
				size: new google.maps.Size(32, 32)
			}
		};

		markerArray = [];
		map = new $window.google.maps.Map(document.getElementById('map_tokyo'),
			mapOptions);
		pointArray = new google.maps.MVCArray();
		socialPointArray = new google.maps.MVCArray();

		var newsGradient = [
			'rgba(255, 228, 0, 0)',
			'rgba(255, 228, 0, 1)',
			'rgba(255, 186, 0, 1)',
			'rgba(255, 124, 0, 1)',
			'rgba(255, 80, 0, 1)',
			'rgba(255, 45, 0, 1)',
			'rgba(255, 0, 0, 1)'
		];
		var socialGradient = [
			// 'rgba(0, 66, 255, 0)',
			// 'rgba(0, 157, 255, 1)',
			// 'rgba(0, 232, 255, 1)',
			// 'rgba(0, 255, 165, 1)',
			// 'rgba(0, 255, 66, 1)'
			'rgba(255, 28, 211, 0)',
			'rgba(255, 28, 211, 0.8)',
			'rgba(255, 28, 211, 1)',
			'rgba(183, 19, 255, 1)',
			'rgba(183, 27, 255, 1)',
			//'rgba(183, , 255, 1)',
		];
		socialHeatMap = new google.maps.visualization.HeatmapLayer({
			data: socialPointArray,
			gradient: socialGradient,
			maxIntensity: 10,
			opacity: 0.8
		});
		socialHeatMap.setMap(map);
		socialHeatMap.set('radius', 10);
		heatMap = new google.maps.visualization.HeatmapLayer({
			data: pointArray,
			gradient: newsGradient
		});
		heatMap.setMap(map);
		heatMap.set('radius', 50);

		$window.google.maps.event.addListenerOnce(map, 'tilesloaded', function(){
			$scope.$emit('MapReady');
		});
		
		var currentDisaster = 0;
		var currentArticle = 0;
		var currentTweet = 0;

		$scope.$on('dataReady', function(){
			var articles = $scope.articles;
			var disasters = $scope.disasters;
			var tweets = $scope.tweets;
			var disastersLength = $scope.disasters.length;
			var articlesLength = $scope.articles.length;
			var tweetsLength = $scope.tweets.length;
			var addArticlePoint = function(point){
				pointArray.push(new google.maps.LatLng(point.lat, point.lon));
			};
			var currentInfoWindow = null;
			var disasterTypes = {
				'isolated': '孤立',
				'blackout': '停電'
			};
			var setInfoWindow = function(disaster, marker) {
				var infowindow = new google.maps.InfoWindow({
					content: String(disasterTypes[disaster.type] + ': ' + disaster.address)
				});
				google.maps.event.addListener(marker, 'click', function(){
					if(currentInfoWindow) {
						currentInfoWindow.close();
					}
					infowindow.open(map, this);
					currentInfoWindow = infowindow;
					$scope.$emit('disasterSelected', this.disaster);
					$scope.$apply();
				});
			};
			$scope.$watch('time', function(newValue){
				for(var i = currentArticle; i < articlesLength; i++) {
					var article = articles[i];
					if(article.pubTime < newValue) {
						angular.forEach(article.points, addArticlePoint);
						currentArticle++;
					}
				}
				for(var j = currentDisaster; j < disastersLength; j++) {
					var disaster = disasters[j];
					if(disaster.time < newValue) {
						var marker = new google.maps.Marker({
							position: new google.maps.LatLng(disaster.lat, disaster.lon),
							title: disaster.type + disaster.address,
							icon: markerIconUrls[disaster.type],
							map: map
						});
						marker.disaster = disaster;
						setInfoWindow(disaster, marker);
						markerArray.push(marker);
						currentDisaster++;
					}
				}
				for(var k = currentTweet; k < tweetsLength; k++) {
					var tweet = tweets[k];
					if(tweet.time < newValue) {
						socialPointArray.push(new google.maps.LatLng(tweet.lat, tweet.lon));
						currentTweet++;
						// var circleOptions = {
						// 	strokeColor: '#FF0000',
						// 	strokeOpacity: 0.4,
						// 	strokeWeight: 1,
						// 	fillColor: '#FF0000',
						// 	fillOpacity: 0.2,
						// 	map: map,
						// 	center: new google.maps.LatLng(tweet.lat, tweet.lon),
						// 	radius: 20
						// };
						// new google.maps.Circle(circleOptions);
					}
				}
				$rootScope.currentDisaster = currentDisaster;
				$rootScope.currentArticle = currentArticle;
				$rootScope.currentTweet = currentTweet;
			});
		});
		$scope.$on('resetTimeline', function(){
			angular.forEach(markerArray, function(marker){
				marker.setMap(null);
			});
			markerArray = [];
			pointArray.clear();
			socialPointArray.clear();
			currentDisaster = 0;
			currentArticle = 0;
			currentTweet = 0;
		});
	};
	initMap();
});