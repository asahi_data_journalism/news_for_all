'use strict';
angular.module('newsForAllWebApp')
.controller('MainCtrl', function($window, $timeout, $scope, $rootScope, $http, newsTimeline, disasterTimeline, socialTimeline) {
	var startTime = (new Date('Fri, 14 Feb 2014 00:00:00')).getTime();
	var endTime = (new Date('Sat, 1 Mar 2014 00:00:00')).getTime();
	var fps = 10;
	var tick = 3600000;
	$scope.startTime = startTime;
	$scope.endTime = endTime;
	$scope.time = startTime;
	$scope.frame = 0;
	var frameGap = 1000/fps;
	$scope.play = false;
	$scope.currentDate = new Date(startTime);
	$scope.selectedDisaster = null;

	$rootScope.currentDisasters = 0;
	$rootScope.currentArticles = 0;
	$rootScope.currentTweets = 0;

	/*$scope.$watch('sliderTime', function(newValue){
		$scope.stopTimeline();
		$scope.time = newValue;
	});*/

	var tickTimeline = function(){
		if($scope.time < endTime && $scope.play) {
			$scope.time += tick;
			if($scope.time > endTime) {
				$scope.time = endTime;
			}
			$scope.currentDate.setTime($scope.time);
			$scope.frame++;
			$scope.sliderTime = $scope.time;
			$timeout(tickTimeline, frameGap);
		}
	};

	$scope.$on('MapReady', function(){
		disasterTimeline.getEvents().then(function(data){
			$scope.disasters = data;
			angular.forEach($scope.disasters, function(disaster){
				disaster.time = (new Date(disaster.date)).getTime();
			});
			newsTimeline.getArticles().then(function(data){
				$scope.articles = data;
				angular.forEach($scope.articles, function(artilce){
					artilce.pubTime = (new Date(artilce.published_at)).getTime();
				});
				socialTimeline.getTweets().then(function(tweets){
					$scope.tweets = tweets;
					angular.forEach($scope.tweets, function(tweet){
						tweet.time = (new Date(tweet.date)).getTime();
					});
					$scope.$broadcast('dataReady');
				}, function(){
					$scope.tweets = [];
				});
			}, function(reason){
				console.error(reason);
			});
			//$scope.playTimeline();
		}, function(error){
			console.error(error);
		});
	});

	$scope.playTimeline = function(){
		if(!$scope.play) {
			//$scope.time = angular.copy($scope.sliderTime);
			$scope.play = true;
			tickTimeline();
		}
	};

	$scope.stopTimeline = function(){
		$scope.play = false;
	};

	$scope.resetTimeline = function(){
		$scope.play = false;
		$scope.time = startTime;
		$scope.currentDate = new Date(startTime);
		$scope.frame = 0;
		$scope.sliderTime = startTime;
		$scope.$broadcast('resetTimeline');
	};

	$scope.$on('disasterSelected', function(evnt, disaster){
		$scope.selectedDisaster = disaster;
		console.log(disaster);
	});
});
