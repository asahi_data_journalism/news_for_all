# -*- coding: utf-8 -*-

import BeautifulSoup

def remove_space_bet_nl(text):
  if isinstance(text, str):
    text=text.encode('utf-8')
  while text.find(u'\n ')>=0:
    text=text.replace(u'\n ', u'\n')
  while text.find(u'\n　')>=0:
    text=text.replace(u'\n　', u'\n')
  return text

def extract_text(body):
    '''HTMLを渡すとtextを抜き出します'''
    if not body:
      return ""
    readable_article=body.replace("<p>","[test]")
    readable_article=readable_article.replace("</p>","[test]")
    readable_article=readable_article.replace("<br />","[test]")
    readable_article=readable_article.replace("<br/>","[test]")
    readable_article=readable_article.replace("<br>","[test]")
    readable_article=readable_article.replace("</h3>","[test]</h3>")
    soup = BeautifulSoup.BeautifulSoup(readable_article)
    texts = soup.getText()
    texts = remove_space_bet_nl(texts)
    texts = texts.replace('[test]', '\n\n')
    texts = texts.replace('\n\n\n\n', '\n\n')
    texts = texts.replace('\n\n\n', '\n\n')
    return texts
