# -*- coding: utf-8; -*-

from asahi_model import configure as configure_asahi
from asahi_model import get_isolated_area, get_blackout_area, set_lon_lat_for_blackout_area, set_lon_lat_for_isolated_area
from asahi_model import get_pickup_contents, set_contents_location

from content_model import configure as configure_prod
from content_model import get_content

from wakati import extract_location
from text_extractor import extract_text

from location import lon_lat

if __name__=="__main__":
    from sqlalchemy import create_engine
    from db import MySQL_conf
    sql_conf_asahi=MySQL_conf('asahi')
    sql_conf_prod=MySQL_conf('prod')

    '''
    # for isolated area dat
    configure_asahi(create_engine(sql_conf_asahi.orm_config))
    for i in get_isolated_area():
        id,pref, city, street=i
        ll=lon_lat(pref+city+street)[0]
        print (pref+city+street).encode('utf-8'), ll
        set_lon_lat_for_isolated_area(id, ll['lon'], ll['lat'], ll['mlevel'])

    # for blackout data
    configure_asahi(create_engine(sql_conf_asahi.orm_config))
    for i in get_blackout_area():
        id,pref, city, street=i
        ll=lon_lat(pref+city+street)[0]
        print (pref+city+street).encode('utf-8'), ll
        set_lon_lat_for_blackout_area(id, ll['lon'], ll['lat'], ll['mlevel'])
    '''

    #print get_pickup_contents(1)
    #print set_contents_location(2492203662805800401, 'test', None, 1, 1, 'TEST')

    configure_asahi(create_engine(sql_conf_asahi.orm_config))
    configure_prod(create_engine(sql_conf_prod.orm_config))

    #MAX=10
    #counter=0
    for i in get_pickup_contents(1):
        topic_id, contents_id=i
        print '----------------', contents_id
        title, body=get_content(contents_id)
        text=extract_text(title+body)
        print text

        locations=extract_location(text)
        for loc in locations:
            print loc
            ll_raw=lon_lat(loc)
            if not ll_raw: continue
            ll=ll_raw[0]
            print ll['address'], ll['lon'], ll['lat'], ll['mlevel']
            set_contents_location(contents_id, ll['address'], None, ll['lon'], ll['lat'], ll['mlevel'])
        #if counter>MAX: break
        #else: counter+=1
