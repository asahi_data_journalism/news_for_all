# -*- coding: utf-8; -*-

import json
import urllib2

import pprint as pp

HOST="http://localhost"

def query(data, port, path, host=HOST):
  req = urllib2.Request(host+":"+str(port)+"/"+path+"/",
          json.dumps(data), {'Content-Type': 'application/json'})
  f = urllib2.urlopen(req)
  j = json.loads(f.read())
  return j

def extract_location(text):
  data={'text':text}
  wakati = query(data, 4444, 'juman', HOST)
  lock=False
  word=''
  word_loc=''
  locs=[]
  for i in  wakati:
    word=i[0][0]
    type=i[1][2]
    if word=='@': continue
    #print word, type
    if not (type in [u'地名', u'名詞性特殊接尾辞'] or word==u'・'):
      lock=False
      if word_loc: 
        locs.append(word_loc)
        #print '-----------------found', word_loc
        word_loc=''
    else:
      if word :
        if not lock and type==u'地名':
          word_loc=word
          lock=True
        elif lock:
          word_loc+=word.replace(u'・',' ')
  if word_loc: 
    locs.append(word_loc)
    word_loc=''
    #print '-----------------found', word_loc
  return locs

#print extract_location("大雪被害が出ている埼玉県秩父市が自衛隊派遣を要請したのに県が拒否した問題で、県が同市などから上がってきた情報だけを頼りに>    したため、孤立集落の実態把握が遅れ、派遣要請を断っていたことが２０日、わかった")


#with open('test_article.txt', 'rb') as f:
  #for i in  extract_location(f.read()):
