#!/usr/bin/env python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------
#
#                ,,~~--___---,         Written by
#               /             .~,
#           /  _,~             )         Akira Shibata
#          (_-(~)   ~, ),,,(  /'           for Shiroyagi Corp.
#          Z6  .~`' ||     \ |　　　　
#          /_,/     ||      ||               akira.shibata@shiroyagi.co.jp
#    ~~~~~~~~~~~~~~~W`~~~~~~W`~~~~~~~~~
#
#----------------------------------------------------------------------------

import sqlalchemy as sa
from sqlalchemy import Column
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import and_, func

from shiroyagi_conf.Model.ping import ping_connection

#from scope import Scope

#scope = Scope()
DBSession = scoped_session(sessionmaker(autocommit=False,
                                 autoflush=True,
                                 expire_on_commit=False))
                                 #scopefunc=scope.get)

Base = declarative_base()
Base.query = DBSession.query_property()

#DBSession = scoped_session(sessionmaker())
#Base = declarative_base()

def configure(engine):
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine

##
## model classes
##

class PickupContents(Base):
    __tablename__ = 'pickup_contents'

    topic_id=Column(sa.Integer, primary_key=True)
    contents_id=Column(sa.Integer)
    date_published=Column(sa.DateTime)
    created_at=Column(sa.DateTime)

class ContentsLocation(Base):
    __tablename__ = 'contents_location'

    id=Column(sa.Integer, primary_key=True)
    contents_id=Column(sa.Integer)
    area_name_src=Column(sa.String)
    code=Column(sa.String)
    lon=Column(sa.Integer)
    lat=Column(sa.Integer)
    level=Column(sa.String)

    def __init__(self, contents_id, area_name, code, lon, lat, level):
        self.contents_id=contents_id
        self.area_name_src=area_name
        self.code=code
        self.lon=lon
        self.lat=lat
        self.level=level

class IsolatedArea(Base):
    __tablename__ = 'isolated_area'

    id=Column(sa.Integer, primary_key=True)
    pref=Column(sa.String)
    city=Column(sa.String)
    street=Column(sa.String)
    num_house=Column(sa.Integer)
    num_people=Column(sa.Integer)
    lon=Column(sa.Float)
    lat=Column(sa.Float)
    level=Column(sa.String)

    def __init__(self, id, pref, city, street, num_house, num_people, lon, lat, level):
      self.id=id
      self.pref=pref
      self.city=city
      self.street=street
      self.num_house=num_house
      self.num_people=num_people
      self.lon=lon
      self.lat=lat
      self.level=level

class BlackoutArea(Base):
    __tablename__ = 'blackout_area'

    id=Column(sa.Integer, primary_key=True)
    pref=Column(sa.String)
    city=Column(sa.String)
    street=Column(sa.String)
    num_house=Column(sa.Integer)
    num_people=Column(sa.Integer)
    lon=Column(sa.Float)
    lat=Column(sa.Float)
    level=Column(sa.String)

    def __init__(self, id, pref, city, street, num_house, num_people, lon, lat, level):
      self.id=id
      self.pref=pref
      self.city=city
      self.street=street
      self.num_house=num_house
      self.num_people=num_people
      self.lon=lon
      self.lat=lat
      self.level=level

def get_isolated_area():
    row = [IsolatedArea.id, IsolatedArea.pref, IsolatedArea.city, IsolatedArea.street ]
    try:
      query=DBSession.query(*row)
      out=query.all()
    except:
      DBSession.rollback()
      print 'error yielding isolated area'
      return None
    if len(out)==0:
      print 'error yielding isolated area'
      return None
    else:
      return out

def get_blackout_area():
    row = [BlackoutArea.id, BlackoutArea.pref, BlackoutArea.city, BlackoutArea.street ]
    try:
      query=DBSession.query(*row)
      out=query.all()
    except:
      DBSession.rollback()
      print 'error yielding blackout area'
      return None
    if len(out)==0:
      print 'error yielding blackout area'
      return None
    else:
      return out

    topic_id=Column(sa.Integer, primary_key=True)
    contents_id=Column(sa.Integer)
    date_published=Column(sa.DateTime)
    created_at=Column(sa.DateTime)

def get_pickup_contents(topic_id=1):
    row = [PickupContents.topic_id, PickupContents.contents_id]
    query=DBSession.query(*row)
    try:
      out=query.filter(PickupContents.topic_id==topic_id).all()
    except:
      DBSession.rollback()
      print 'error pickup contents'
      return None
    if len(out)==0:
      print 'error pickup contents area'
      return None
    else:
      return out

def set_contents_location(contents_id, area_name, code, lon, lat, level):
    item=ContentsLocation(contents_id, area_name, code, lon, lat, level)
    try:
        DBSession.merge(item)
        DBSession.commit()
    except:
        DBSession.rollback()
        print "something went wrong adding a record to list", contents_id

def set_lon_lat_for_isolated_area(id, lon, lat, level):
    row=[IsolatedArea.id,IsolatedArea.lon,IsolatedArea.lat,IsolatedArea.level]
    try:
        query=DBSession.query(*row)
        query.filter(IsolatedArea.id==id).update({'lon':lon, 'lat':lat, 'level':level})
        DBSession.commit()
    except:
        print "something went wrong adding a record"
        DBSession.rollback()
        raise

def set_lon_lat_for_blackout_area(id, lon, lat, level):
    row=[BlackoutArea.id,BlackoutArea.lon,BlackoutArea.lat,BlackoutArea.level ]
    try:
        query=DBSession.query(*row)
        query.filter(BlackoutArea.id==id).update({'lon':lon, 'lat':lat, 'level':level})
        DBSession.commit()
    except:
        print "something went wrong adding a record"
        DBSession.rollback()
        raise


if __name__=='__main__':
    import datetime
    from db import MySQL_conf
    sql_conf=MySQL_conf('asahi')

    from sqlalchemy import create_engine

    configure(create_engine(sql_conf.orm_config))

    #print get_isolated_area()
    #print get_blackout_area()
    #set_lon_lat_for_blackout_area(1, 1,1,'SHK')
    #set_lon_lat_for_isolated_area(1, 1,1,'SHK')

    print get_pickup_contents(1)
    #print set_contents_location(2492203662805800401, 'test', None, 1, 1, 'TEST')
