#!/usr/bin/env python
# -*- coding: utf-8 -*-

#----------------------------------------------------------------------------
#
#                ,,~~--___---,         Written by
#               /             .~,
#           /  _,~             )         Akira Shibata
#          (_-(~)   ~, ),,,(  /'           for Shiroyagi Corp.
#          Z6  .~`' ||     \ |　　　　
#          /_,/     ||      ||               akira.shibata@shiroyagi.co.jp
#    ~~~~~~~~~~~~~~~W`~~~~~~W`~~~~~~~~~
#
#----------------------------------------------------------------------------

import sqlalchemy as sa
from sqlalchemy import Column
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import and_, func

from shiroyagi_conf.Model.ping import ping_connection

#from scope import Scope

#scope = Scope()
DBSession = scoped_session(sessionmaker(autocommit=False,
                                 autoflush=True,
                                 expire_on_commit=False))
                                 #scopefunc=scope.get)

Base = declarative_base()
Base.query = DBSession.query_property()

#DBSession = scoped_session(sessionmaker())
#Base = declarative_base()

def configure(engine):
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine

##
## model classes
##

class ContentsRss(Base):
    __tablename__ = 'CONTENTS_RSS'

    contents_id=Column(sa.Integer, primary_key=True)
    title=Column(sa.String)
    body=Column(sa.String)
    link=Column(sa.String)
    source_id=Column(sa.Integer)
    date_added=Column(sa.DateTime)
    date_body_added=Column(sa.DateTime)
    date_published=Column(sa.DateTime)
    img=Column(sa.String)
    img_cache=Column(sa.String)
    ban_score=Column(sa.Integer)
    duplicate_cid=Column(sa.Integer)

def get_content(cid):
    row = [ContentsRss.title, ContentsRss.body]
    try:
      query=DBSession.query(*row)
      out=query.filter(ContentsRss.contents_id==cid).all()
    except:
      DBSession.rollback()
      print 'not found:', cid
      raise
      return None
    if len(out)==0:
        print 'not found:', cid
        return None
    else:
        return out[0]

if __name__=='__main__':
    import datetime
    from db import MySQL_conf
    sql_conf=MySQL_conf('prod')

    from sqlalchemy import create_engine

    configure(create_engine(sql_conf.orm_config))
    print get_content(2534682355506470601)
