#!/usr/bin/env python
# -*- coding: utf-8 -*-

from shiroyagi_server.DbIo.cursor_retreiver import get_con_cursor
from datetime import datetime

con, cursor=get_con_cursor('prod')

#with open('source_names.txt', 'wb') as f:
#  for i in cursor.fetchall():
#    print str(i[0])+','+i[1].encode('utf-8')+','+str(i[2])
#    f.write(str(i[0])+','+i[1].encode('utf-8')+','+str(i[2])+'\n')

date_from=datetime(2014, 2, 13)
date_to=datetime(2014, 2, 24)

with open('articles.txt', 'wb') as f:
  f.write('contents_id, source_id, date_published, title, link, body\n')
  for i in [647, 908, 909, 910, 914, 917, 929, 580, 581, 935, 729, 731]:
    cursor.execute('select contents_id, source_id, date_published, title, link, body from CONTENTS_RSS where source_id=%s and date_published>"%s" and date_published<"%s"'%(i, date_from, date_to))
    for j in cursor.fetchall():
      data=[]
      for k in j:
        if isinstance( k, unicode):
          data.append(k.encode('utf-8').replace('\n',''))
        else:
          data.append(str(k))
      print data
      f.write( ','.join(data)+'\n')
