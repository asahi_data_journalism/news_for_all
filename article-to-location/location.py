# -*- coding: utf-8; -*-

import urllib
import requests
import xml.etree.ElementTree as et
import pdb
from copy import copy

NG=[u'日本',u'関西', u'関東',u'太平洋', u'日', u'東海', u'東海道', u'九州', u'近畿']
MOD=[u'KEN',u'SHK',u'OAZ', u'AZC',u'GIK',u'TBN',u'EBN']

def lcs(xstr, ystr):
    """
    >>> lcs('thisisatest', 'testing123testing')
    'tsitest'
    """
    if not xstr or not ystr:
        return ""
    x, xs, y, ys = xstr[0], xstr[1:], ystr[0], ystr[1:]
    if x == y:
        return x + lcs(xs, ys)
    else:
        return max(lcs(xstr, ys), lcs(xs, ystr), key=len)

def lcs_front(xstr, ystr):
  counter=0
  cs=''
  min_len=min(len(xstr), len(ystr))
  for i in range(min_len):
    if xstr[i]==ystr[i]:
      cs+=xstr[i]
    else:
      break
  return cs

def geo_average(l):
  ave={}
  n=0
  for i in l:
    if not ave:
      ave=copy(i)
      n=1
    else:
      ave['address']=lcs_front(ave['address'], i['address'])
      ave['lon']+=float(i['lon'])
      ave['lat']+=float(i['lat'])
      n+=1
  
  ave['lon']=ave['lon']/n
  ave['lat']=ave['lat']/n

  if not ave['address'] :
    return []

  if ave['address'][0]==u'県' or ave['address'][0]==u'市' or ave['address'][0]==u'区':
    return []
  
  return [ave]
      

def higher_level(lvla, lvlb):
  if not lvla: return lvlb
  if not lvlb: return lvla
  if MOD.index(lvla)<MOD.index(lvlb):
    return lvla
  else:
    return lvlb

def lon_lat(name, average=True):
  if isinstance(name, unicode):
    name_uni=name
    name_str=name.encode('utf-8')
  else:
    name_uni=name.decode('utf-8')
    name_str=name
  if name_uni in NG: 
    return None

  name_quote=urllib.quote(name_str)
  url= 'http://test.cgi.e-map.ne.jp/cgi/searchaddr.cgi?key=37nQ8zCTmwQzAXnwLnBCnARf9JmgTPBHjgBzDZngUiZOnQhytQngKT4angrr50pRK8JYoRnLFVpRqzTC&enc=UTF8&datum=WGS84&pflg=1&frewd='+name_quote+'&outf=XML&mlflg=1'
  r = requests.get(url)
  text=r.text.encode('utf-8')
  #print '========================', name
  #print text
  root = et.fromstring(text)

  l=[]
  for item in root.findall('result')[0].findall('item'):
    l.append( {'address': item.findall('address')[0].text, 'lon': float(item.findall('lon')[0].text), 'lat':float(item.findall('lat')[0].text),
              'mlevel':item.findall('matchLevelName')[0].text})
  
  m_high=u''
  for i in l:
    m_high=higher_level(m_high, i['mlevel'])

  l_filtered=filter(lambda x: x['mlevel']==m_high, l)
  if len(l_filtered)>1 and average:
    l_filtered=geo_average(l_filtered)

  return l_filtered

if __name__=="__main__":

  import argparse 

  parser = argparse.ArgumentParser(description=u'find coordinates of given location')
  parser.add_argument('-l', type=str, help=u'location', default=None, dest='location', action='store', required=False)
  parser.add_argument('--average', help=u'totake average', default=True, dest='average', action='store_true', required=False)

  args = parser.parse_args()

  location = args.location
  location=u"富士"

  geo= lon_lat(location, args.average)
  #print geo

  for i in geo:
    print i['address'].encode('utf-8'), i['lon'], i['lat'], i['mlevel']
