# -*- coding: utf-8; -*-

from text_extractor import extract_text

from wakati import extract_location
from location import lon_lat

text=u"大雪被害が出ている埼玉県秩父市が自衛隊派遣を要請したのに県が拒否した問題で、県が同市などから上がってきた情報だけを頼りにしたため、孤立集落の実態把握が遅れ、派遣要請を断っていたことが２０日、わかった"


if __name__=='__main__':
  
  with open('test_article2.txt', 'rb') as f:
    text=extract_text(f.read())

  print text

  for i in extract_location(text):
    ll= lon_lat(i)
    if not ll: continue
    print '-------------------------',i,'----------------------------'
    for j in ll:
      print j['address'], j['lon'], j['lat']
