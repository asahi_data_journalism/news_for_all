var mysql = require('mysql');
var m_conf = require('../conf');
var dateformat = require('dateformat');
var s_conf = require('../../../shiroyagi_web/conf/conf_prod_bizzlio');

//console.log(s_conf.MAIN_DB);

var pools = {};

// MySQL
var getPool = function(conf, callback) {

  var uuid = m_conf.createDBURI(conf);

  var pool = pools[uuid];

  if (!pool) {
    console.log('db pool is not exist, created:', conf.host + '/' + conf.database);
    pool = pools[uuid] = mysql.createPool(conf);
  }

  return callback(null, pool);

};


exports.getConnection = function(conf, callback) {
  getPool(conf, function(error, pool){
    if (error) return callback(error);
    pool.getConnection(callback);
  });

};

var destroyPool = function(pool) {
  pool.getConnection(function(error, conn) {
    if (error || !conn) return console.error("pool destroy error2:", error);
    if (conn) {
      conn.release();
      return conn.destroy();
    }
  });
};

exports.destroyPool = function(conf) {

  var uuid = m_conf.createDBURI(conf);
  var pool = pools[uuid];

  if (pool) {
    destroyPool(pool);
  }
};

exports.destroyAllPool = function() {
  for(var k in pools) {
    var pool = pools[k];
    destroyPool(pool);
  }
};

exports.castBigInt = function (field, next) {
  //console.log(field.type);
  if (field.type === 'LONGLONG') return field.string();
  return next();
};

function manipulateContentResults (rows) {
  // add image_path
  rows.forEach(function(el){

    if (el.img_cache) {
      var first = el.img_cache.split(',')[0];
      if (first) {
        // /article_img/20131231/cid_0.(jpg|png)
        var utcDate = new Date(el.date_added.getTime()); // UTC
        el.image_url = 'http://image.bizzlio.com/article_img/' + dateformat(utcDate, "yyyymmdd", true) + "/" + el.contents_id + "_" + first;
      }
    }
  });
};

function fetchContentsLocation(self, srcContents, callback) {

  var cids = srcContents.map(function(row){
    return row.contents_id;
  });


  self.getConnection(m_conf.ASAHI_DB, function(error, conn){

    if (error) return callback(error);

    conn.query({sql:'select * from contents_location where contents_id in (?)', typeCast:self.castBigInt}, [cids], function(error, rows){

      conn.release();

      if (error) return callback(error);

      var contents = {}; // map by contents_id
      srcContents.forEach(function(c){
        contents[c.contents_id+""] = c;
      });

      rows.forEach(function(row){
        var c = contents[row.contents_id+""];
        c.points.push(row);
        row.contents_id = undefined; // destroy/trim id
      });

      return callback(null, srcContents);

    });

  });

};

exports.manipulateContents = function(cids, callback) {

  var self = this;

  if (cids.length === 0) return callback(null, []);

  this.getConnection(s_conf.MAIN_DB, function(error, conn){

    if (error) return callback(error);

    conn.query({sql:'select img_cache,date_added,date_body_added,source_name,CONTENTS_RSS.source_id,contents_id,title,link as url,date_published,date_published as published_at from CONTENTS_RSS inner join CONTENTS_SOURCE on CONTENTS_RSS.source_id = CONTENTS_SOURCE.source_id where contents_id in (?)', typeCast:self.castBigInt}, [cids], function(error, rows){

      conn.release();

      if (error) return callback(error);

      rows = rows.sort(function(a,b){
        return a.date_published - b.date_published;
      });

      rows.forEach(function(row){
        row.points = [];
        row.favicon_url = "http://image.bizzlio.com/favicon/" + row.source_id + ".ico"
      });

      manipulateContentResults(rows);

      //return callback(null, rows);

      fetchContentsLocation(self, rows, callback);

    });


  });
};