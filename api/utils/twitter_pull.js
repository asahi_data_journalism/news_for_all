var Reader = require('./tw_reader');

var db = require('../lib/db');
var conf = require('../conf');

/*var r = new Reader('out.txt');
r.on('end', function(){
  console.log('end');
});
r.on('data', function(data){
  console.log("%j",data.coordinates);
  //console.log("%s",data["id_str"]);
});
r.on('error', function(error){
  console.error(error);
});
r.start();
  */

const MIN_REL = process.argv[3] || 4;

var file = process.argv[2];

console.log("processing... %s", file);

function insertToDB(data, lon, lat, point) {

  db.getConnection(conf.ASAHI_DB, function(error, conn){

    if(error) return console.error(error);

    var insertData = {
      tw_id:data.id_str,
      user_id:data.user && data.user.id_str,
      screen_name:data.user && data.user.screen_name,
      text: data.text,
      date: new Date(data.created_at),
      relevance: point,
      file: file,
      lon: lon,
      lat: lat

    };

    conn.query('insert into twitter_geo_stream set ?', insertData, function(error, stat){

      conn.release();

      if(error) return console.error(error);

      console.log('inserted %s', data.id_str);


    });

  });
}

function calcRelevance(data) {
  var mainTerm = '雪';

  var refTerms = ["被害","停電","孤立","閉鎖","買い物","記録","事故","車中泊","遅れ","運休","立ち往生","出られない","断念",
    "足止め","寸断","乱れ","死者","骨折","倒木","転倒","搬送","中止","延期","巻き込まれ","見合わせ","中止","崩落","水漏れ","死",
    "通行止","に影響","安否","不通","通行できな","食料","燃料","繋がらない", "つながらない","動けない","困難"];

  var point = 0;
  var text = data.text;

  if(text.indexOf(mainTerm) >= 0) {
    point += 3;
  }
  refTerms.forEach(function(term){
    if(text.indexOf(term) >= 0) {
      point+=1;
    }
  });

  return point;

};

var g_processCount = 0;

function processTweet(data, lon, lat) {

  g_processCount++;

  if(g_processCount % 100 === 0) {
    console.log("%s, %s", g_processCount, file);
  }

  var point = calcRelevance(data);

  if(point < MIN_REL) {
    return; // skip
  }

  console.log(point, data.text);
  insertToDB(data, lon, lat, point);
};

function parseCoordinates(data) {
  var lon = null, lat = null;


  var bb = data.place && data.place.bounding_box && data.place.bounding_box.coordinates;
  bb = bb && bb[0] && bb[0].length === 4 && bb[0];

  if(!data.coordinates || !data.coordinates.coordinates) {
    //return console.warn("no coordinates: %j", bb);

    if(bb) {
      lon = (parseFloat(bb[0][0]) + parseFloat(bb[1][0])) * 0.5;
      lat = (parseFloat(bb[0][1]) + parseFloat(bb[1][1])) * 0.5;
      //console.log("from bb",lon,lat);
      return processTweet(data, lon ,lat);
    } else {
      return console.warn("no coordinates");
    }
  }
  if(data.coordinates.coordinates.length !== 2) {
    return console.warn("invalid coordinates");
  }

  /*if(data.coordinates && data.coordinates.coordinates) {
    lon = parseFloat(data.coordinates.coordinates[0]);
    lat = parseFloat(data.coordinates.coordinates[1]);
  }*/

  lon = parseFloat(data.coordinates.coordinates[0]);
  lat = parseFloat(data.coordinates.coordinates[1]);


  //console.log(lon,lat);

  return processTweet(data, lon ,lat);

};


function startRead(path) {

  var r = new Reader(path);
  r.on('end', function(){
    console.log('end');
  });
  r.on('data', function(data){
    //console.log("%j",data.coordinates);
    parseCoordinates(data);
    //console.log("%s",data["id_str"]);
  });
  r.on('error', function(error){
    console.error("error:", error);
  });
  r.start();
};



startRead(file);