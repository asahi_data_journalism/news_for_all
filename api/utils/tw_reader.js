var util = require('util'),
  EventEmitter = require('events').EventEmitter;
var fs = require('fs');

var Reader = function(path, params) {
  EventEmitter.call(this);

  this.path = path;
  this.params = params || {};

  this.twitter = null;
  this.request = null;

  // for streaming
  this.chunk = '';

  var self = this;
};

util.inherits(Reader, EventEmitter);


Reader.prototype.start = function() {
  this.sendStreamRequest_();
};

Reader.prototype.prepareRequest_ = function() {

  // destroy current
  this.request = null;
  this.chunk = '';

  this.stream = fs.createReadStream(this.path);
  var self = this;
  this.stream.on('error', function(error){
    self.emit('error', error);
  });

  this.stream.on('data', self.onData_.bind(self));
  this.stream.on('end', function(){
    self.emit('end');
  });
};

// For Streaming API

Reader.prototype.stop = function() {

};

Reader.prototype.sendStreamRequest_ = function() {
//  var self = this;
  this.prepareRequest_();
};

Reader.prototype.onData_ = function(chunk) {
  var self = this;

  // ignore error and data
  // parse chunk myself

  //console.log(chunk);

  self.chunk += chunk;

  while(1) {
    // search LF
    var loc = self.chunk.indexOf("\n");
    if (loc === -1) break; // not found

    var body = self.chunk.slice(0, loc); // slice valid JSON body
    self.chunk = self.chunk.slice(loc+1);

    var bodyArr = body.split('\t');
    if(bodyArr.length !== 2) {
      console.error("chunk error");
      break;
    }

    try {

      var tw_id = bodyArr[0];

      var data = JSON.parse(bodyArr[1].trim());
      return self.emit('data', data);
    } catch (e) {
      self.chunk = ""; // clear current chunk
      return console.error(e, body.trim(), self.chunk);
      break;
    }
  }
};

module.exports = Reader;