var express = require('express'),
  db = require('../lib/db'),
  conf = require('../conf'),
  async = require('async');

var app = express();
module.exports = app;

const MAP_PREFIX = '/map';


app.get('/topics', function(req, res, next){
  db.getConnection(conf.ASAHI_DB, function(error, conn){

    if (error) return next(error);

    conn.query("select id as topic_id,topic_name from topic_master", null, function(error, rows){

      conn.release();

      if(error) return next(error);

      res.json({topics:rows});
    });

  });
});


app.get(MAP_PREFIX + '/news', function(req, res, next){

  var date_start = req.param('date_start');
  var date_end = req.param('date_end');
  if(!date_start || !date_end) {
    return res.send(400);
  }

  date_start = parseInt(date_start);
  date_end = parseInt(date_end);

  var topicId = req.param('topic_id') || 1;

  db.getConnection(conf.ASAHI_DB, function(error, conn){

    if (error) return next(error);

    conn.query({sql:"select * from pickup_contents where topic_id = ? and date_published >= ? and date_published <= ? order by date_published", typeCast:db.castBigInt}, [topicId, new Date(date_start), new Date(date_end)], function(error, rows){

      conn.release();

      if (error) return next(error);

      var cids = rows.map(function(row){
        return row.contents_id+"";
      });

      db.manipulateContents(cids, function(error, results){

        if (error) return next(error);

        res.json({articles:results});

      });
    })


    });

});



app.post(MAP_PREFIX + '/data_by_rect', function(req, res, next){

  var rect = req.body.rect;

  var cls = req.body['class'];

  db.getConnection(conf.ASAHI_DB, function(error, conn){

    if (error) return next(error);

    conn.query("select * from area_master inner join population on area_master.code = population.code where `class` = ? and lon >= ? and lon <= ? and lat >= ? and lat <= ? and year = '2014' ", [cls, rect.lon[0], rect.lon[1], rect.lat[0], rect.lat[1]], function(error, rows){

      conn.release();

      if(error) return next(error);

      res.json({data:rows});

    });

  });

});



app.post(MAP_PREFIX + '/data', function(req, res, next){
  var codes = req.body.areas.map(function(d){
    return d.code+"";
  });

  db.getConnection(conf.ASAHI_DB, function(error, conn){

    if (error) return next(error);

    conn.query("select * from area_master inner join population on area_master.code = population.code where population.code in (?) and year = '2014' ", [codes], function(error, rows){

      conn.release();

      if(error) return next(error);

      var areas = {}; // mapped by code
      rows.forEach(function(row){
        areas[row.code] = row;
      });

      var results = codes.map(function(code){
        var row = areas[code];
        if (row) return row;
        return null;
      });

      res.json({data:results});

    });

  });

});

app.get(MAP_PREFIX + '/disaster', function(req, res, next){

  var tables = ['blackout_area', 'isolated_area', 'snowds_post'];
  var tasks = tables.map(function(tableName){
    return function(next){

      db.getConnection(conf.ASAHI_DB, function(error, conn){

        if (error) return next(error);

        conn.query("select *,CONCAT(pref,city,street) as address from " + tableName + " where enabled = 1", null, function(error, rows){

          conn.release();

          if(error) return next(error);

          rows.forEach(function(row){
            row.type = tableName.split('_')[0];
          });

          return next(null, rows);

        });

      });

    };

  });

  async.parallel(tasks, function(error, resutls){

    if (error) return next(error);

    var all = [];
    resutls.forEach(function(rows){
      all = all.concat(rows);
    });

    all = all.sort(function(a,b){
      return a.date - b.date;
    });

    return res.json({data:all});


  });

  //select *,CONCAT(pref,city,street) as address from

});

app.get(MAP_PREFIX + '/twitter', function(req, res, next){

  var date_start = req.param('date_start');
  var date_end = req.param('date_end');
  if(!date_start || !date_end) {
    return res.send(400);
  }

  date_start = parseInt(date_start);
  date_end = parseInt(date_end);

  var min_relevance = req.param('min_relevance') || 3;

  db.getConnection(conf.ASAHI_DB, function(error, conn){

    if (error) return next(error);

    conn.query({sql:"select * from twitter_geo_stream where relevance >= ? and date >= ? and date <= ? order by date", typeCast:db.castBigInt}, [min_relevance, new Date(date_start), new Date(date_end)], function(error, rows){

      conn.release();

      if (error) return next(error);

      res.json({tweets:rows});

    })


  });

});