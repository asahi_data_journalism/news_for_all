var express = require('express'),
  db = require('../lib/db');



var app = express();
module.exports = app;

const MAP_PREFIX = '/map';


app.get('/topics', function(req, res, next){
  res.json({
    topics:[{
      title: "2/14の降雪",
      id: 1
    }]
  })
});


app.get(MAP_PREFIX + '/news', function(req, res, next){

  var date_start = req.param('date_start');
  var date_end = req.param('date_end');
  if(!date_start || !date_end) {
    return res.send(400);
  }

  var firstDate = new Date("2013/4/6");
  var mockArray = [];
  var base = {
    points:[{lon:136.1234567,lat:36.987654}, {lon:136.1334567,lat:36.777777}],
    contents_id: "2620936529729800201",
    image_url: "http://image.bizzlio.com/article_img/20140227/2620936529729800201_0.jpg",
    url: "http://www.senken.co.jp/news/31883/",
    source_id: 2980,
    source_name: "繊研新聞",
    title: "ユニクロ、スーピマコットンで仕掛け",
    favicon_url:"http://image.bizzlio.com/favicon/2980.ico",
    alter_images:[]
  };
  for(var i = 0;i < 30; i++) {
    var data = {};
    Object.keys(base).map(function(k){
      data[k] = base[k];
    });
    data.date_published = new Date(firstDate.getTime() + 3600*8*1000*i);
    mockArray.push(data);
  }
  res.json({articles:mockArray});

});

app.all(MAP_PREFIX + '/data_by_area', function(req, res, next){

  var mockArray = [];
  var base = {
    code:"12345",
    area_name:"長野県松本市",
  class: "city",
  population: 200000,
  population_aged: 20000,
  lon: 136.123456,
  lat: 36.123456
  }
  for(var i = 0;i < 30; i++) {
    var data = {};
    Object.keys(base).map(function(k){
      data[k] = base[k];
    });
    mockArray.push(data);
  }
  res.json({data:mockArray});

});


app.all(MAP_PREFIX + '/data', function(req, res, next){

  var data = req.body.area;

  var mockArray = [];
  var base = {
    code:"12345",
    area_name:"長野県松本市",
    class: "city",
    population: 200000,
    population_aged: 20000,
    lon: 136.123456,
    lat: 36.123456
  }
  for(var i = 0;i < 30; i++) {
    var data = {};
    Object.keys(base).map(function(k){
      data[k] = base[k];
    });
    if(i!==0&&i%10 === 0) mockArray.push(null);
    mockArray.push(data);
  }
  res.json({data:mockArray});

});
