var env = process.env.NODE_ENV || 'development'
  , cfg = require('./conf_'+env);

cfg.createDBURI = function(dict) {
  return "mysql://" + dict.user + ":" + dict.password + "@" + dict.host + ":" + dict.port + "/" + dict.database;
};

module.exports = cfg;